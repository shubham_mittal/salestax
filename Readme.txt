/* 
Project Name : Sales Tax Problem
Project language : JAVA
Proect Author Name : Shubham Mittal
Project created on : Fri, 10th October 2014
Project Description : To print the receipt after validating different inputs and applying tax accordingly.
*/

Driver package : com.salestax.driver
Driver Class : ItemsInputMenu.java (main class)

 How to run the console application:
1. Compile the main class using Java compiler.
2. Make the entries and follow the instructions.



Limitations and Scope:
Database has not been included.
With inclusion of database of your choice(can be done using jpa, or jdbc, etc), the above project can serve as a taxcalculation portal with little modification in the implemented usage of indentification of item type.



Refer to the snapshots for more information or kindly write to me at shubham9910103545@gmail.com
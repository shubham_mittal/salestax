package com.salextax.data;

import java.util.HashMap;
import java.util.List;

public class SalesTaxEntries {

	private static HashMap<Integer, List<String>> hashMap;

	public static HashMap<Integer, List<String>> getHashMap() {
		return hashMap;
	}

	public static void setHashMap(HashMap<Integer, List<String>> hashMap) {
		if( SalesTaxEntries.hashMap == null)
		{
			SalesTaxEntries.hashMap = hashMap;
		}
		else
		{
			SalesTaxEntries.hashMap.putAll(hashMap);
		}
	}

	
	
	

}

package com.salestax.driver;

import java.io.IOException;



import com.salestax.exception.InvalidInputException;
import com.salestax.menuGenrator.MenuGenrator;
import com.salestax.printer.InputDataExtraction;


public class ItemsInputMenu {

	public void itemsMenu() {

		try {
			new MenuGenrator().menuDetails();

			System.out.println( "\t\t  Genrated Receipt  " );
			System.out.println( "\t\t____________________");
			System.out.println(" ");
			System.out.println("Item Name"+"\t\t\t\t"+"Price(Inc. Tax)");
			new InputDataExtraction().detailsExtractor();
		} catch (NumberFormatException | IOException | InvalidInputException e) {
			System.out
					.println("Unhandled exception caught..Restarting program");
			itemsMenu();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new ItemsInputMenu().itemsMenu();

	}

}

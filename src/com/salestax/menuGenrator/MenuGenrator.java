package com.salestax.menuGenrator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import com.salestax.exception.InvalidInputException;
import com.salestax.service.TaxEntriesTO;
import com.salextax.input.DataInput;

public class MenuGenrator {

	public void menuDetails() throws IOException, InvalidInputException,
			NumberFormatException {

		System.out.println("Please select from Given choices" + "     "
				+ new Date());
		System.out.println("1.Generate Receipt");
		System.out.println("2. Exit");
		int response = -1;
		boolean check = false;
		while (!check) {
			try {
				response = Integer.parseInt(new DataInput().dataScanner());
				check = true;
			} catch (NumberFormatException e) {
				System.out.println("Please enter numerical value.");
			}
			if (response == 1) {
				menuGenerator();
			}

			else if (response == 2) {
				{
					System.exit(0);

				}
			} else {
				System.out.println("Please choose from the given choices");
				check = false;
			}
		}

	}

	public void menuGenerator() {

		try {
			TaxEntriesTO taxEntriesTO = new TaxEntriesTO();
			List<String> list = new ArrayList<String>();
			int entryCount = 1;

			System.out
					.println("Enter the Quantity First, followed by Name and Price");
			System.out.println("example : 1 book at 23");
			System.out.println("Press 0 to interrupt");
			int flag = 1;
			System.out.println("Input " + entryCount + " :");
			do {
				String input = new DataInput().dataScanner();
				if (input.equalsIgnoreCase("0")) {
					if (list.size() == 0) {
						System.out.println("Please enter atleast one item");
						continue;
					}
					boolean outercheck = false;
					while (!outercheck) {
						System.out
								.println("Please Press 1 to add details in a new entry");
						System.out.println("Please Press 2 to print receipt");
						int response = -1;
						boolean check = false;
						while (!check) {
							try {
								response = Integer.parseInt(new DataInput()
										.dataScanner());
								check = true;
							} catch (NumberFormatException e) {
								System.out
										.println("Please enter numerical value");
							}
						}
						if (response == 1) {
							taxEntriesTO.setHashMap(list);
							list = new ArrayList<String>();
							entryCount++;
							System.out.println("Input " + entryCount + " :");
							outercheck = true;

						}

						else if (response == 2) {
							{

								taxEntriesTO.setHashMap(list);
								System.out.println("Printing now");
								outercheck = true;

								return;
							}
						} else {
							System.out
									.println("Please choose from given choices");
							outercheck = false;
						}
					}
				} else {
				      String regex="^([1-9][0-9]*+[\\s]+(([\\w][\\s])*[\\w])+[\\s]+[at]+[\\s]+([0-9]*)+[.]*+[0-9]*)$";
				      if(input.matches(regex)){
					list.add(input);}
				      else{
				    	  while(!(input.matches(regex)))
				    	  {
				    		  System.out.println( "Make Entry In Valid Format" );
				    		  input = new DataInput().dataScanner();
				    	  }
				    	  list.add(input);
				      }
				}
			} while (flag == 1);
		} catch (Exception e) {
			throw e;
		}

	}
}

package com.salestax.printer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.salextax.data.SalesTaxEntries;

public class InputDataExtraction {

	private String itemName;
	private Integer quantitity;
	private double price;
	private double totalCost;
	private double basicSalesTaxTotal;
	private double importSalesTaxTotal;
	private String foodItems="chocolate";
	private String medicalItem="pill";
	private String bookItem="book";
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Integer getQuantitity() {
		return quantitity;
	}
	public void setQuantitity(Integer quantitity) {
		this.quantitity = quantitity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public double getBasicSalesTaxTotal() {
		return basicSalesTaxTotal;
	}
	public void setBasicSalesTaxTotal(double basicSalesTaxTotal) {
		this.basicSalesTaxTotal = basicSalesTaxTotal;
	}
	public double getImportSalesTaxTotal() {
		return importSalesTaxTotal;
	}
	public void setImportSalesTaxTotal(double importSalesTaxTotal) {
		this.importSalesTaxTotal = importSalesTaxTotal;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	
	
	public void detailsExtractor(){
		
		HashMap<Integer, List<String>> hashMap = new HashMap<Integer, List<String>>();
		hashMap = SalesTaxEntries.getHashMap();
		String detail[];
		int position=0;
		int lastIndex;
		for (int i = 0; i < hashMap.size(); i++) {
			List<String> list = new ArrayList<String>();
			list = hashMap.get(i + 1);
			Iterator<String> itr = list.iterator();

			while (itr.hasNext()) {
				String details=itr.next();
				detail=details.split(" ",2);
				this.setQuantitity(Integer.parseInt(detail[position]));
				
				lastIndex=detail[position+1].lastIndexOf("at ");
				
				this.setItemName(detail[position+1].substring(0, lastIndex-1));
				
				this.setPrice(Double.parseDouble(detail[position+1].substring((lastIndex+3))));
				computeTax();
			}
			
	}
		System.out.println("Total Cost :"+"\t\t\t\t"+this.getTotalCost());
		System.out.println("Total Basic Sales Tax :"+"\t\t\t\t"+new DecimalFormat("#0.00").format(this.getBasicSalesTaxTotal()));
		System.out.println("Total Import Sales Tax :"+"\t\t\t"+this.getImportSalesTaxTotal());
	}
	
	public void computeTax()
	{	double price=0.0;
		double price1=0.0;
		if(!(this.getItemName().toLowerCase().contains(this.foodItems) || this.getItemName().toLowerCase().contains(this.bookItem) || this.getItemName().toLowerCase().contains(this.medicalItem)))
		{	
			price1= this.getPrice()*0.1;
			
			this.setBasicSalesTaxTotal(this.getBasicSalesTaxTotal()+(this.getQuantitity()*Math.round(price1*20.0)/20.0));
			price1=Math.round(price1*20.0)/20.0;
			
			
		}
		
		if(this.itemName.toLowerCase().contains("import")){
			
			price=this.getPrice()*0.05;
			
			this.setImportSalesTaxTotal(this.getImportSalesTaxTotal()+(this.getQuantitity()*Math.round(price*20.0)/20.0));
			price= Math.round(price*20.0)/20.0;
			
			
		}
		
			this.setPrice(this.getPrice()+price1+price);
		
			
			this.setTotalCost(this.getTotalCost()+(this.getQuantitity()*this.getPrice()));
			printDetails();
	}
	
	public void printDetails(){
		
		
		System.out.println( this.getItemName()+"\t\t\t\t"+ new DecimalFormat("#0.00").format(this.getPrice()) );
		
		
	}
}
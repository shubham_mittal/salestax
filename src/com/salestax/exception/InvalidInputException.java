package com.salestax.exception;

@SuppressWarnings("serial")
public class InvalidInputException extends Exception {

	public InvalidInputException() {
		super( "Invalid Input, Please Enter Again" );
	}

}
